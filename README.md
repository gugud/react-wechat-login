#### Install
npm install --registry=http://dev.gugud.com:10080 --save react-wechat-login@0.0.6

#### Props

```
Component.propTypes = {
  qrcodeUrl: PropTypes.string,
  isQrcodeReady: PropTypes.bool.isRequired,
  isLogined: PropTypes.bool.isRequired,
  redirectTo: PropTypes.string.isRequired,
  requestQrcode: PropTypes.func.isRequired
}
```

1. redirectTo: URL, redirected to url when login successed
1. requestQrcode: Redux dispatch action, triggered when component mounted

#### Compile

```
npm install
npm run deploy
npm publish
```
