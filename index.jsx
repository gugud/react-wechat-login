import React from 'react'
import { render } from 'react-dom'
import App from './src/component.jsx'

const data = {
  waitingQrcode: {
    isQrcodeReady: false,
    isLogined: false
  },
  readyQrcode: {
    isQrcodeReady: true,
    isLogined: false,
    qrcodeUrl: "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQH37zoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0JFaHl0enZsa0Y3cE9JRzZRV2F0AAIEM1LWVgMEgDoJAA=="
  },
  loginSuccess: {
    isLogined: true
  }
}


class Page extends React.Component {
  render() {
    return (
      <div>
        <App {...data.waitingQrcode} />
        <hr />
        <App {...data.readyQrcode} />
        <hr />
        <App {...data.loginSuccess} />
      </div>
    )
  }
}

render(<Page />, document.getElementById("home"))