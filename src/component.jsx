import React, { PropTypes } from 'react'

class App extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { requestQrcode } = this.props
    requestQrcode()
  }

  componentWillReceiveProps(nextProps) {
    const { redirectTo } = this.props
    if (nextProps.isLogined) {
      this.context.router.push(redirectTo)
    }
  }

  render() {
    const { qrcodeUrl, isQrcodeReady, isLogined } = this.props
    return (!isLogined &&
      <div style={{
        maxWidth: '204px',
        fontFamily: `"LiHei Pro Medium", "Microsoft YaHei"`
      }}>
        <div style={{
          textAlign: 'center'
        }}>
          {isQrcodeReady &&
            <div style={{
              border: '1px solid #ddd'
            }}>
              <img src={qrcodeUrl} style={{width: '200px', margin: 'auto', padding: '0'}}></img>
            </div>
          }
        </div>
        <div style={{
          textAlign: 'center',
          paddingTop: '8px'
        }}>
          {!isQrcodeReady &&
            <span>正在生成二维码，请稍候 ...</span>
          }
          {isQrcodeReady &&
            <span style={{fontWeight: 'bold'}}>微信“扫一扫”</span>
          }
        </div>
      </div>
    )
  }
}

App.propTypes = {
  qrcodeUrl: PropTypes.string,
  isQrcodeReady: PropTypes.bool.isRequired,
  isLogined: PropTypes.bool.isRequired,
  redirectTo: PropTypes.string.isRequired,
  requestQrcode: PropTypes.func.isRequired
}

App.contextTypes = {
  router: React.PropTypes.object.isRequired
}

export default App