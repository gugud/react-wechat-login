var webpack = require('webpack')

module.exports = {
    entry: [
      'webpack/hot/only-dev-server',
      './index.jsx'
    ],
    output: {
        path: './dist',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
              test: /\.jsx$/,
              loader: 'babel',
              exclude: /node_modules/,
              query: {
                presets: ['es2015', 'react']
              }
            }
        ]
    },
    resolve:{
        extensions:['', '.js', '.jsx', '.json']
    },
    plugins: [
      new webpack.NoErrorsPlugin(),
    ]
}
