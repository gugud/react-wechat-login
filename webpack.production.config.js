var path = require('path');
var webpack = require('webpack');

var config = {
  devtool: 'eval',
  entry: [
    path.resolve(__dirname, 'src/component.jsx'),
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'react-wechat-login.js',
    library: 'react-wechat-login',
    libraryTarget: 'umd'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      }
    ]
  },
  externals: {
    "react": {
      root: 'React',
      commonjs2: 'react',
      commonjs: 'react',
      amd: 'react'
    }
  }
};

module.exports = config;
